#include "TStack.h"
#include <iostream>

TStack::TStack(int Size) : Top(-1), TDataRoot(Size)
{}

void TStack::Put(const TData &Val)
{
	if (pMem == NULL) SetRetCode(DataNoMem);
	else
		if (IsFull()) SetRetCode(DataFull);
	else
	{
		pMem[++Top] = Val;
		DataCount++;
	}
}

TData TStack::Get(void)
{
	TData result = -1;
	if (pMem == NULL) SetRetCode(DataNoMem);
	else
		if (IsEmpty()) SetRetCode(DataEmpty);
	else
	{
		result = pMem[Top--];
		DataCount--;
	}
	return result;
}

void TStack::Print()
{
	for (int i = 0; i < DataCount; i++)
		cout << pMem[i] << ' ';
	cout << endl;
}

int TStack::GetCount()
{
	return DataCount;
}
